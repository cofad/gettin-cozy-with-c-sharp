﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using IpLibrary;

namespace IpWebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class IpController : ControllerBase
    {
        private readonly ILogger<IpController> _logger;

        public IpController(ILogger<IpController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult<Ip>> Get()
        {
            Ip ip = await IpService.GetIp();
            _logger.LogInformation($"IP Address: {ip.Address}");
            return Ok(ip);
        }
    }
}
