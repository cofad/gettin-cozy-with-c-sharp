# NuGet restore
FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /src
COPY *.sln .
COPY IpLibrary/*.csproj IpLibrary/
COPY IpWebAPI/*.csproj IpWebAPI/
COPY WriteIpAddressToFileConsoleApp/*.csproj WriteIpAddressToFileConsoleApp/
RUN dotnet restore
COPY . .

# publish
FROM build AS publish
WORKDIR /src/IpWebAPI
RUN dotnet publish -c Release -o /src/publish

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS runtime
WORKDIR /app
COPY --from=publish /src/publish .

# Localhost
#ENTRYPOINT ["dotnet", "IpWebAPI.dll"]

# Heroku
RUN adduser herokuuser
USER herokuuser
CMD ASPNETCORE_URLS=http://*:$PORT dotnet IpWebAPI.dll

# Localhost commands
# docker build -t -ip-web-api .
# docker run -t -p 80:80 ip-web-api

# Heroku commands
# heroku container:login
# heroku container:push --app gettin-cozy-with-c-sharp --context-path . web
# heroku container:release --app gettin-cozy-with-c-sharp web
# heroku logs --tail --app gettin-cozy-with-c-sharp
