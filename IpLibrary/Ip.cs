﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace IpLibrary
{
    // Provides a more flexible model that can be generated from ApiIp
    public class Ip
    {
        public Ip(string address, string message) { Address = address; Message = message; }
        public string Address { get; private set; }
        public string Message { get; set; }
    }

    // Models the exact response from the API
    class ApiIp
    {
        public string Ip;
    }

    // Service that provides ability to get user's IP from an API
    public class IpService
    {
        private static readonly HttpClient client = new HttpClient();
        private static readonly string apiUrl = "https://api.ipify.org/?format=json";

        public static async Task<Ip> GetIp()
        {
            string json = await client.GetStringAsync(apiUrl);
            ApiIp ApiIp = JsonConvert.DeserializeObject<ApiIp>(json);
            Ip ip = new Ip(ApiIp.Ip, "test message");
            return ip;
        }
    }
}
