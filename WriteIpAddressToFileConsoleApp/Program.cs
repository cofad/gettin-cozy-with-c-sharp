﻿using System.Threading.Tasks;
using IpLibrary;
using System;
using Newtonsoft.Json;
using System.IO;
using System.Net.Http;

namespace WriteIpAddressToFile
{
    class Program
    {
        static readonly string filenamePrefix = "my-ip";
        static readonly string filenameExtension = "json";

        static async Task Main(string[] args)
        {
            try
            {
                Program.DeleteIpJsonFiles();
                await Program.WriteIpJsonToFile();
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("Error retrieving IP!");
            }
        }

        static void DeleteIpJsonFiles()
        {
            var dir = new DirectoryInfo(Directory.GetCurrentDirectory());

            foreach (var file in dir.EnumerateFiles($"{filenamePrefix}*.{filenameExtension}"))
            {
                file.Delete();
            }
        }

        static async Task WriteIpJsonToFile()
        {
            string filename = Program.GetFilename();
            string ipJson = await Program.GetSerializedIp();
            File.WriteAllText(filename, ipJson);
        }

        static string GetFilename()
        {
            string date = DateTime.Now.ToString("yyyy-MM-dd");
            return $"{Program.filenamePrefix}-{date}.{Program.filenameExtension}";
        }

        static async Task<string> GetSerializedIp()
        {
            Ip ip = await IpService.GetIp();
            return JsonConvert.SerializeObject(ip);
        }
    }
}
